import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс-клиент, взаимодействующий с сервером.
 * Получает с сервера арифметическое бинарное выражение,
 * позволяет ввести с клавиатуры результат решения выражения и отправлет его серверу.
 */
public class Client {

    /**
     * Метод обмена сообщениями с серером.
     * Получает с сервера арифметическое бинарное выражения,
     * позволяет ввести с клавиатуры результат решения выражения и отправить его серверу.
     */
    public static void main(String[] args) {
        try (Socket clientSocket = new Socket("localhost", 9125)) {
            try (InputStream inputStream = clientSocket.getInputStream();
                 OutputStream outputStream = clientSocket.getOutputStream();
                 DataInputStream input = new DataInputStream(inputStream);
                 DataOutputStream output = new DataOutputStream(outputStream)) {
                Scanner in = new Scanner(System.in);
                String answer;
                String results = "";
                boolean checker;
                while (true) {
                    System.out.println(input.readUTF());
                    for (int i = 0; i < 3; i++) {
                        do {
                            System.out.println("Введите ответ для " + (i+1) + " выражения");
                            answer = in.nextLine();
                            checker = isNum(answer);

                            if (!checker) {
                                System.out.println("Ответ должен содержать только целые числа");
                            }
                        } while (!checker);
                        results = results + answer + ",";
                    }
                    output.writeUTF(results);
                    output.flush();

                }
            }
        } catch (IOException e) {
            System.out.println("*** Socket ERROR *** \n" + e);
        }
    }

    /**
     * Метод проверки полученной строки на корректность.
     *
     * @param string Результат решения арифметического бинарного выражения.
     * @return true, если строка представляет собой число,
     * не выходящее за рамки допустимых значений типа Int, иначе false.
     */
    private static boolean isNum(String string) {
        Pattern pattern = Pattern.compile("-?[\\d]{1,9}");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}