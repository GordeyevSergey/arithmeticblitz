import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Класс-сервер, взаимодействующий с клиентом.
 * Генерирует арифметическое бинарное выражение,
 * отправляет выражение клиенту,
 * проверяет полученный от клиента результат решения выражения,
 * подсчитывает время с момента генерации выражения до момента получения верного результата решения.
 */
public class Server {

    /**
     * Метод, ожидающий подключения клиента.
     */
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(9125)) {
            System.out.println("Ожидание клиента");
            try (Socket socket = serverSocket.accept()) {
                System.out.println("Клиент " + socket.getInetAddress() + " подключен");
                messaging(socket);
            }
        } catch (IOException e) {
            System.out.println("ERROR \n " + e);
        }
    }

    /**
     * Метод обмена сообщениями с клиентом.
     * Осуществляет подсчет времени решения клиентом арифметического бинарного выражения.
     * Ожидает получения результата решения выражения со стороны клиента,
     * отправляет клиенту сообщение о правильности решения выражения.
     *
     * @param socket сокет подключенного клиента.
     */
    private static void messaging(Socket socket) {
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream();
             DataInputStream input = new DataInputStream(inputStream);
             DataOutputStream output = new DataOutputStream(outputStream)) {

            String[] expressionAndResult;
            String answer;
            long solutionTime;
            int number = 3; // кол-во генерируемых выражений
            while (true) {
                solutionTime = System.currentTimeMillis();

                expressionAndResult = concatenation(number).split(":");
                output.writeUTF(expressionAndResult[0]);

                answer = input.readUTF();

                output.writeUTF(succesfulToString(numberOfCorrectAnswers(expressionAndResult[1],answer),number) +
                        "\nВремя прохождения блиц-викторины:"+ (System.currentTimeMillis() - solutionTime) / 1000 + " секунд \n");
                output.flush();
            }
        } catch (IOException e) {
            System.out.println("ERROR \n " + e);
        }
    }

    /**
     *
     * @param trueAnswers количество верных ответов
     * @param num количество сгенерированных выражений
     * @return строка, содержащая в себе статистику блиц-викторины и
     */
    private static String succesfulToString(int trueAnswers, int num){
        return "Верных ответов (" + trueAnswers + "/" + num + ")";
    }

    /**
     * Метод определяет количество верных ответов на блиц-викторину
     * @param results результаты решения сгенерированных бинарных выражений
     * @param answers результаты решения бинарных выражений пользователем
     * @return количество верных ответов.
     */
    private static int numberOfCorrectAnswers(String results, String answers){
        String[] resultArray = results.split(",");
        String[] answerArray = answers.split(",");

        int count = 0;
        for (int i = 0; i < resultArray.length; i++){
            if (resultArray[i].equals(answerArray[i])){
                count++;
            }
        }
        return count;
    }



    /**
     * Метод генерации арифметического бинарного выражения,
     * а также подсчета результата его решения.
     * Выражение от результата отделяется знаком ","
     *
     * @return сгенерированное выражение и результат его решения (Например, 2 * 2 =, 4)
     */
    private static String generator() {
        int operand1 = (int) (Math.random() * 100);
        int operand2 = (int) (Math.random() * 100);
        switch ((int) (Math.random() * 4)) {
            case 1:
                return operand1 + " + " + operand2 + " =," + (operand1 + operand2);
            case 2:
                return operand1 + " - " + operand2 + " =," + (operand1 - operand2);
            case 3:
                return operand1 + " * " + operand2 + " =," + (operand1 * operand2);
            case 0:
                return operand1 + " / " + operand2 + " =," + (int) (operand1 / operand2);
            default:
                return "1 + 1 =,2";
        }
    }

    /**
     *
     * @param number
     * @return
     */
    private static String concatenation(int number){
        String[] strings;
        StringBuilder expressions = new StringBuilder();
        StringBuilder results = new StringBuilder();
        for (int i = 0; i < number; i++) {
            strings = generator().split(",");
            for (String string : strings) {
                try {
                    results.append(Integer.parseInt(string)).append(",");
                } catch (NumberFormatException e) {
                    expressions.append("\n").append(string);
                }
            }
        }
        return expressions + ":" + results;
    }

}